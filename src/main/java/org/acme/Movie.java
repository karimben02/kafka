package org.acme;

public class Movie {

    public String title;
    public int year;
    public int price;

    @Override
    public String toString() {
        return "Film :" + '\'' +
                "Le titre est :'" + title + '\'' +
                ", le prix du film: " + price + " €"+ '\'' +
                ", ce film est sortie en: " + year
                ;
    }

}
